# Sample app

This is a sample Rails application to get started with an 
[Elabs](https://gitlab.com/experimentslabs/engine_elabs) website. 

**NOTE** that the gem is not yet released, so you have to use a relative path to elabs if you plan to use this.

**NOTE that the commit tree may change until 2.0.0 is out.**

## Setup

**Pre-requisites:** Postgresqsl, yarn, ffmpeg, imagemagick.

- Clone this repository
- Install dependencies:
  - `bundle install`
  - `yarn install`
- Configure the database access: copy `config/database.sample.yml` to `database.yml`
  and edit it`

You're done, run `rails s` and visit http://localhost:3000

## Notes

- The master key is versionned, please change it if you want to deploy this somewhere.
- For bugs related to Elabs engine, check the [issues](https://gitlab.com/experimentslabs/engine_elabs/issues) on the Elabs engine repo.
- For issues with this app, open them [here](https://gitlab.com/experimentslabs/sample_app/issues).
