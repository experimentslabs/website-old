Rails.application.routes.draw do
  # Elabs routes
  mount Elabs::Engine, at: '/'

  # Home page (or whatever suits your needs. I.e.: 'elabs/projects#index', ...)
  root 'elabs/acts#index'
end
