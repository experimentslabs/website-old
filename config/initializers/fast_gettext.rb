FastGettext.add_text_domain 'app', path: File.expand_path('../../locale', __dir__), type: :po
FastGettext.default_available_locales = %w[en fr] # all you want to allow
FastGettext.default_text_domain = 'app'
