# Don't forget to restart the server when you edit this file.
Elabs.setup do |config|
  # Site name, used in menus and other places
  config.site_name = 'ExperimentsLabs'

  # Maximum number of items per public "index" pages
  config.max_items_per_page = 25

  # Maximum number of items per admin "index" pages
  config.max_admin_items_per_page = 50

  # Maximum number of items per member "index" pages
  config.max_members_items_per_page = 25

  # Maximum number related content items in "show" views
  config.max_related_items = 10

  # Number of uploads thumbnails to show on albums cards
  config.albums_max_shown_uploads = 3

  # List of audio formats that can be used with html5 players
  config.av_formats_audio = ['audio/mpeg', 'audio/ogg', 'audio/wav']

  # List of video formats that can be used with html5 players
  config.av_formats_video = ['video/mp4', 'video/webm', 'video/ogg']

  # ActiveStorage validations are not supported. This leads to saving
  # the uploaded file even if validation fails. To prevent this, the
  # attached file is deleted on failure, resulting on avatar suppression.
  # Set this value to false to avoid using avatars
  config.use_avatars = true

  # Open/close the registrations
  config.users_can_register = true

  # Enable the form honeypot used for reports/comments
  config.trap_dumb_bots = true
end
