puts "# RewriteEngine  on"

[
    { model: Elabs::Language, subs: %w[albums articles notes projects uploads] },
    { model: Elabs::License, subs: %w[albums articles notes projects uploads] },
    { model: Elabs::Tag, subs: %w[albums articles notes projects uploads] },
    { model: Elabs::User, subs: %w[albums articles notes projects uploads] },
    { model: Elabs::Album, subs: [] },
    { model: Elabs::Article, subs: [] },
    { model: Elabs::Note, subs: [] },
    { model: Elabs::Project, subs: %w[albums articles notes uploads] },
    { model: Elabs::Upload, subs: [] },
].each do |config|
  slug_field = config[:model]::SLUG_FIELD
  endpoint   = config[:model].to_s.demodulize.tableize
  puts '# Entities redirection'
  config[:model].all.each do |record|
    puts "Redirect 301 /#{endpoint}/#{record.id} /#{endpoint}/#{record.send(slug_field)}"

    config[:subs].each do |sub|
      puts "Redirect 301 /#{endpoint}/#{record.id}/#{sub} /#{endpoint}/#{record.send(slug_field)}/#{sub}"
    end
  end
end

