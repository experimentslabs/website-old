# "Your email address has been successfully confirmed."
N_('devise.confirmations.confirmed')
# "You will receive an email with instructions for how to confirm your email address in a few minutes."
N_('devise.confirmations.send_instructions')
# "If your email address exists in our database, you will receive an email with instructions for how to confirm your email address in a few minutes."
N_('devise.confirmations.send_paranoid_instructions')
# "You are already signed in."
N_('devise.failure.already_authenticated')
# "Your account is not activated yet."
N_('devise.failure.inactive')
# "Invalid %{authentication_keys} or password."
N_('devise.failure.invalid')
# "Your account is locked."
N_('devise.failure.locked')
# "You have one more attempt before your account is locked."
N_('devise.failure.last_attempt')
# "Invalid %{authentication_keys} or password."
N_('devise.failure.not_found_in_database')
# "Your session expired. Please sign in again to continue."
N_('devise.failure.timeout')
# "You need to sign in or sign up before continuing."
N_('devise.failure.unauthenticated')
# "You have to confirm your email address before continuing."
N_('devise.failure.unconfirmed')
# "Confirmation instructions"
N_('devise.mailer.confirmation_instructions.subject')
# "Reset password instructions"
N_('devise.mailer.reset_password_instructions.subject')
# "Unlock instructions"
N_('devise.mailer.unlock_instructions.subject')
# "Email Changed"
N_('devise.mailer.email_changed.subject')
# "Password Changed"
N_('devise.mailer.password_change.subject')
# "Could not authenticate you from %{kind} because \"%{reason}\"."
N_('devise.omniauth_callbacks.failure')
# "Successfully authenticated from %{kind} account."
N_('devise.omniauth_callbacks.success')
# "You can't access this page without coming from a password reset email. If you do come from a password reset email, please make sure you used the full URL provided."
N_('devise.passwords.no_token')
# "You will receive an email with instructions on how to reset your password in a few minutes."
N_('devise.passwords.send_instructions')
# "If your email address exists in our database, you will receive a password recovery link at your email address in a few minutes."
N_('devise.passwords.send_paranoid_instructions')
# "Your password has been changed successfully. You are now signed in."
N_('devise.passwords.updated')
# "Your password has been changed successfully."
N_('devise.passwords.updated_not_active')
# "Bye! Your account has been successfully cancelled. We hope to see you again soon."
N_('devise.registrations.destroyed')
# "Welcome! You have signed up successfully."
N_('devise.registrations.signed_up')
# "You have signed up successfully. However, we could not sign you in because your account is not yet activated."
N_('devise.registrations.signed_up_but_inactive')
# "You have signed up successfully. However, we could not sign you in because your account is locked."
N_('devise.registrations.signed_up_but_locked')
# "A message with a confirmation link has been sent to your email address. Please follow the link to activate your account."
N_('devise.registrations.signed_up_but_unconfirmed')
# "You updated your account successfully, but we need to verify your new email address. Please check your email and follow the confirm link to confirm your new email address."
N_('devise.registrations.update_needs_confirmation')
# "Your account has been updated successfully."
N_('devise.registrations.updated')
# "Signed in successfully."
N_('devise.sessions.signed_in')
# "Signed out successfully."
N_('devise.sessions.signed_out')
# "Signed out successfully."
N_('devise.sessions.already_signed_out')
# "You will receive an email with instructions for how to unlock your account in a few minutes."
N_('devise.unlocks.send_instructions')
# "If your account exists, you will receive an email with instructions for how to unlock it in a few minutes."
N_('devise.unlocks.send_paranoid_instructions')
# "Your account has been unlocked successfully. Please sign in to continue."
N_('devise.unlocks.unlocked')
# "was already confirmed, please try signing in"
N_('errors.messages.already_confirmed')
# "needs to be confirmed within %{period}, please request a new one"
N_('errors.messages.confirmation_period_expired')
# "has expired, please request a new one"
N_('errors.messages.expired')
# "not found"
N_('errors.messages.not_found')
# "was not locked"
N_('errors.messages.not_locked')
# "1 error prohibited this %{resource} from being saved:"
N_('errors.messages.not_saved.one')
# "%{count} errors prohibited this %{resource} from being saved:"
N_('errors.messages.not_saved.other')
