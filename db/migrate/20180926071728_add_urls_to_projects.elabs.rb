# This migration comes from elabs (originally 20180917133442)
class AddUrlsToProjects < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :sources_url, :string
    add_column :projects, :docs_url,    :string
  end
end
