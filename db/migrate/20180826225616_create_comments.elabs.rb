# This migration comes from elabs (originally 20180813000006)
class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.references :content, polymorphic: true
      t.string :name
      t.string :email
      t.text :comment
      t.boolean :allow_contact
      t.references :user, foreign_key: true
      t.boolean :deleted

      t.timestamps
    end
  end
end
