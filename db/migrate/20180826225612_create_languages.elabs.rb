# This migration comes from elabs (originally 20180813000001)
class CreateLanguages < ActiveRecord::Migration[5.2]
  def change
    create_table :languages do |t|
      t.string :iso639_1, size: 2,  default: nil, null: false
      t.string :name,     size: 45, default: nil, null: false
      t.integer :albums_count,   default: 0, null: false
      t.integer :articles_count, default: 0, null: false
      t.integer :notes_count,    default: 0, null: false
      t.integer :projects_count, default: 0, null: false
      t.integer :uploads_count,  default: 0, null: false

      t.index :iso639_1, unique: true
      t.index :name,     unique: true
    end
  end
end
