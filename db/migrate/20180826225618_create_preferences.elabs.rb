# This migration comes from elabs (originally 20180813000008)
class CreatePreferences < ActiveRecord::Migration[5.2]
  def change
    create_table :preferences do |t|
      t.references :user, foreign_key: true
      t.boolean :show_nsfw, null: false, default: false
      t.string  :locale,    null: true,  default: nil
    end

    Elabs::User.all.each do |user|
      Preference.create! user: user, show_nsfw: false
    end
  end
end
