# This migration comes from elabs (originally 20181030063505)
class CreateAnnouncements < ActiveRecord::Migration[5.2]
  def change
    create_table :announcements do |t|
      t.datetime :start_at
      t.datetime :end_at
      t.text :content, null: false, default: nil
      t.string :level, null: false, default: nil
      t.string :target, null: false, default: 'global'
      t.references :user, foreign_key: true, null: false

      t.timestamps
    end
  end
end
