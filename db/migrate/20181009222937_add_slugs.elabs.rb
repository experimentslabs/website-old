# This migration comes from elabs (originally 20181006073541)
class AddSlugs < ActiveRecord::Migration[5.2]
  def up
    %i[licenses tags albums articles notes projects uploads].each do |table|
      add_column table, :slug, :string, null: true
    end

    ActiveRecord::Base.record_timestamps = false
    begin
      [
        { model: Elabs::License, actable: false },
        { model: Elabs::Tag, actable: false },
        { model: Elabs::Album, actable: true },
        { model: Elabs::Article, actable: true },
        { model: Elabs::Note, actable: true },
        { model: Elabs::Project, actable: true },
        { model: Elabs::Upload, actable: true }
      ].each do |table|
        table[:model].all.each do |record|
          record.fill_slug
          record.minor_update = true if table[:actable]
          record.save!
        end
      end
    ensure
      ActiveRecord::Base.record_timestamps = true
    end

    %i[licenses tags albums articles notes projects uploads].each do |table|
      change_column table, :slug, :string, null: false
      add_index table, :slug, unique: true
    end
  end

  def down
    %i[licenses tags albums articles notes projects uploads].each do |table|
      remove_column table, :slug
    end
  end
end
