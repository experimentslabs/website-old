# This migration comes from elabs (originally 20180813000000)
class DeviseUpdateUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :username, :string, null: false
    add_column :users, :real_name, :string
    add_column :users, :biography, :text
    add_column :users, :role, :string, null: false, default: 'user'
    add_column :users, :albums_count,   :integer, default: 0, null: false
    add_column :users, :articles_count, :integer, default: 0, null: false
    add_column :users, :notes_count,    :integer, default: 0, null: false
    add_column :users, :projects_count, :integer, default: 0, null: false
    add_column :users, :uploads_count,  :integer, default: 0, null: false

    add_index :users, :username, unique: true
  end
end
