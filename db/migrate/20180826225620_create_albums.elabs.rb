# This migration comes from elabs (originally 20180813000010)
class CreateAlbums < ActiveRecord::Migration[5.2]
  def change
    create_table :albums do |t|
      t.string :name,               null: false, default: nil
      t.text :description,          null: false, default: nil
      t.boolean :sfw,               null: false, default: false
      t.boolean :published,         null: false, default: false
      t.boolean :locked,            null: false, default: false
      t.boolean :hidden_in_history, null: false, default: false
      t.integer :uploads_count,     null: false, default: 0

      t.references :user,     foreign_key: true
      t.references :license,  foreign_key: true
      t.references :language, foreign_key: true

      t.datetime :published_at,     null: true
      t.timestamps
    end

    create_join_table :albums, :tags
    add_index :albums_tags, %i[album_id tag_id], unique: true
  end
end
