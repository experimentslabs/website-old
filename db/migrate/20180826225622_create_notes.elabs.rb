# This migration comes from elabs (originally 20180813000012)
class CreateNotes < ActiveRecord::Migration[5.2]
  def change
    create_table :notes do |t|
      t.text :content,              null: false, default: nil
      t.boolean :sfw,               null: false, default: false
      t.boolean :published,         null: false, default: false
      t.boolean :locked,            null: false, default: false
      t.boolean :hidden_in_history, null: false, default: false

      t.references :user,     foreign_key: true
      t.references :license,  foreign_key: true
      t.references :language, foreign_key: true

      t.datetime :published_at,     null: true
      t.timestamps
    end

    create_join_table :notes, :tags
    add_index :notes_tags, %i[note_id tag_id], unique: true
  end
end
