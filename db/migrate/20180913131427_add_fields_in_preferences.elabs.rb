# This migration comes from elabs (originally 20180913093027)
class AddFieldsInPreferences < ActiveRecord::Migration[5.2]
  def change
    add_reference :preferences, :writing_language, table: :languages, index: { name: :writing_language_preferences_index }
    add_reference :preferences, :writing_license, table: :licenses, index: { name: :writing_license_preferences_index }
  end
end
