# This migration comes from elabs (originally 20180910000015)
class RemoveLicenseTimestamps < ActiveRecord::Migration[5.2]
  def change
    remove_column :licenses, :created_at
    remove_column :licenses, :updated_at
  end
end
