# This migration comes from elabs (originally 20181005105642)
class AddReasonToActs < ActiveRecord::Migration[5.2]
  def change
    add_column :acts, :reason, :string, default: nil, null: true
  end
end
