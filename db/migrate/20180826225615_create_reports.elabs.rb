# This migration comes from elabs (originally 20180813000005)
class CreateReports < ActiveRecord::Migration[5.2]
  def change
    create_table :reports do |t|
      t.string :name
      t.string :email
      t.string :url, null: false, default: nil
      t.text :reason, null: false, default: nil
      t.boolean :allow_contact

      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
