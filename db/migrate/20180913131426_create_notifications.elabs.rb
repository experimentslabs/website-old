# This migration comes from elabs (originally 20180910175012)
class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.references :content, polymorphic: true
      t.string :event
      t.belongs_to :user
      t.references :source_user, table: :users, index: { name: :source_user_index }
      t.string :message, default: nil, null: true

      t.timestamps
    end
  end
end
