# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_11_21_225314) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "acts", force: :cascade do |t|
    t.string "content_type"
    t.bigint "content_id"
    t.string "event"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "reason"
    t.index ["content_type", "content_id"], name: "index_acts_on_content_type_and_content_id"
  end

  create_table "albums", force: :cascade do |t|
    t.string "name", null: false
    t.text "description", null: false
    t.boolean "sfw", default: false, null: false
    t.boolean "published", default: false, null: false
    t.boolean "locked", default: false, null: false
    t.boolean "hidden_in_history", default: false, null: false
    t.integer "uploads_count", default: 0, null: false
    t.bigint "user_id"
    t.bigint "license_id"
    t.bigint "language_id"
    t.datetime "published_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug", null: false
    t.index ["language_id"], name: "index_albums_on_language_id"
    t.index ["license_id"], name: "index_albums_on_license_id"
    t.index ["slug"], name: "index_albums_on_slug", unique: true
    t.index ["user_id"], name: "index_albums_on_user_id"
  end

  create_table "albums_tags", id: false, force: :cascade do |t|
    t.bigint "album_id", null: false
    t.bigint "tag_id", null: false
    t.index ["album_id", "tag_id"], name: "index_albums_tags_on_album_id_and_tag_id", unique: true
  end

  create_table "albums_uploads", id: false, force: :cascade do |t|
    t.bigint "album_id", null: false
    t.bigint "upload_id", null: false
    t.index ["album_id", "upload_id"], name: "index_albums_uploads_on_album_id_and_upload_id", unique: true
  end

  create_table "announcements", force: :cascade do |t|
    t.datetime "start_at"
    t.datetime "end_at"
    t.text "content", null: false
    t.string "level", null: false
    t.string "target", default: "global", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_announcements_on_user_id"
  end

  create_table "articles", force: :cascade do |t|
    t.string "title", null: false
    t.string "excerpt", null: false
    t.text "content", null: false
    t.boolean "sfw", default: false, null: false
    t.boolean "published", default: false, null: false
    t.boolean "locked", default: false, null: false
    t.boolean "hidden_in_history", default: false, null: false
    t.bigint "user_id"
    t.bigint "license_id"
    t.bigint "language_id"
    t.datetime "published_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug", null: false
    t.index ["language_id"], name: "index_articles_on_language_id"
    t.index ["license_id"], name: "index_articles_on_license_id"
    t.index ["slug"], name: "index_articles_on_slug", unique: true
    t.index ["user_id"], name: "index_articles_on_user_id"
  end

  create_table "articles_tags", id: false, force: :cascade do |t|
    t.bigint "article_id", null: false
    t.bigint "tag_id", null: false
    t.index ["article_id", "tag_id"], name: "index_articles_tags_on_article_id_and_tag_id", unique: true
  end

  create_table "comments", force: :cascade do |t|
    t.string "content_type"
    t.bigint "content_id"
    t.string "name"
    t.string "email"
    t.text "comment"
    t.boolean "allow_contact"
    t.bigint "user_id"
    t.boolean "deleted"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "archived", default: false, null: false
    t.index ["content_type", "content_id"], name: "index_comments_on_content_type_and_content_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "languages", force: :cascade do |t|
    t.string "iso639_1", null: false
    t.string "name", null: false
    t.integer "albums_count", default: 0, null: false
    t.integer "articles_count", default: 0, null: false
    t.integer "notes_count", default: 0, null: false
    t.integer "projects_count", default: 0, null: false
    t.integer "uploads_count", default: 0, null: false
    t.index ["iso639_1"], name: "index_languages_on_iso639_1", unique: true
    t.index ["name"], name: "index_languages_on_name", unique: true
  end

  create_table "licenses", force: :cascade do |t|
    t.string "name", null: false
    t.string "url", null: false
    t.string "tldr_url"
    t.string "icon", null: false
    t.integer "albums_count", default: 0, null: false
    t.integer "articles_count", default: 0, null: false
    t.integer "notes_count", default: 0, null: false
    t.integer "projects_count", default: 0, null: false
    t.integer "uploads_count", default: 0, null: false
    t.string "slug", null: false
    t.index ["slug"], name: "index_licenses_on_slug", unique: true
  end

  create_table "notes", force: :cascade do |t|
    t.text "content", null: false
    t.boolean "sfw", default: false, null: false
    t.boolean "published", default: false, null: false
    t.boolean "locked", default: false, null: false
    t.boolean "hidden_in_history", default: false, null: false
    t.bigint "user_id"
    t.bigint "license_id"
    t.bigint "language_id"
    t.datetime "published_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug", null: false
    t.index ["language_id"], name: "index_notes_on_language_id"
    t.index ["license_id"], name: "index_notes_on_license_id"
    t.index ["slug"], name: "index_notes_on_slug", unique: true
    t.index ["user_id"], name: "index_notes_on_user_id"
  end

  create_table "notes_tags", id: false, force: :cascade do |t|
    t.bigint "note_id", null: false
    t.bigint "tag_id", null: false
    t.index ["note_id", "tag_id"], name: "index_notes_tags_on_note_id_and_tag_id", unique: true
  end

  create_table "notifications", force: :cascade do |t|
    t.string "content_type"
    t.bigint "content_id"
    t.string "event"
    t.bigint "user_id"
    t.bigint "source_user_id"
    t.string "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["content_type", "content_id"], name: "index_notifications_on_content_type_and_content_id"
    t.index ["source_user_id"], name: "source_user_index"
    t.index ["user_id"], name: "index_notifications_on_user_id"
  end

  create_table "preferences", force: :cascade do |t|
    t.bigint "user_id"
    t.boolean "show_nsfw", default: false, null: false
    t.string "locale"
    t.bigint "writing_language_id"
    t.bigint "writing_license_id"
    t.index ["user_id"], name: "index_preferences_on_user_id"
    t.index ["writing_language_id"], name: "writing_language_preferences_index"
    t.index ["writing_license_id"], name: "writing_license_preferences_index"
  end

  create_table "projects", force: :cascade do |t|
    t.string "name", null: false
    t.string "short_description", null: false
    t.text "description", null: false
    t.string "main_url"
    t.boolean "sfw", default: false, null: false
    t.boolean "published", default: false, null: false
    t.boolean "locked", default: false, null: false
    t.boolean "hidden_in_history", default: false, null: false
    t.integer "albums_count", default: 0, null: false
    t.integer "articles_count", default: 0, null: false
    t.integer "notes_count", default: 0, null: false
    t.integer "uploads_count", default: 0, null: false
    t.bigint "user_id"
    t.bigint "license_id"
    t.bigint "language_id"
    t.datetime "published_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "sources_url"
    t.string "docs_url"
    t.string "slug", null: false
    t.index ["language_id"], name: "index_projects_on_language_id"
    t.index ["license_id"], name: "index_projects_on_license_id"
    t.index ["slug"], name: "index_projects_on_slug", unique: true
    t.index ["user_id"], name: "index_projects_on_user_id"
  end

  create_table "projects_albums", id: false, force: :cascade do |t|
    t.bigint "project_id", null: false
    t.bigint "album_id", null: false
    t.index ["project_id", "album_id"], name: "index_projects_albums_on_project_id_and_album_id", unique: true
  end

  create_table "projects_articles", id: false, force: :cascade do |t|
    t.bigint "project_id", null: false
    t.bigint "article_id", null: false
    t.index ["project_id", "article_id"], name: "index_projects_articles_on_project_id_and_article_id", unique: true
  end

  create_table "projects_notes", id: false, force: :cascade do |t|
    t.bigint "project_id", null: false
    t.bigint "note_id", null: false
    t.index ["project_id", "note_id"], name: "index_projects_notes_on_project_id_and_note_id", unique: true
  end

  create_table "projects_tags", id: false, force: :cascade do |t|
    t.bigint "project_id", null: false
    t.bigint "tag_id", null: false
    t.index ["project_id", "tag_id"], name: "index_projects_tags_on_project_id_and_tag_id", unique: true
  end

  create_table "projects_uploads", id: false, force: :cascade do |t|
    t.bigint "project_id", null: false
    t.bigint "upload_id", null: false
    t.index ["project_id", "upload_id"], name: "index_projects_uploads_on_project_id_and_upload_id", unique: true
  end

  create_table "reports", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "url", null: false
    t.text "reason", null: false
    t.boolean "allow_contact"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_reports_on_user_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name", null: false
    t.integer "albums_count", default: 0, null: false
    t.integer "articles_count", default: 0, null: false
    t.integer "notes_count", default: 0, null: false
    t.integer "projects_count", default: 0, null: false
    t.integer "uploads_count", default: 0, null: false
    t.string "slug", null: false
    t.index ["name"], name: "index_tags_on_name", unique: true
    t.index ["slug"], name: "index_tags_on_slug", unique: true
  end

  create_table "uploads", force: :cascade do |t|
    t.string "title", null: false
    t.text "description", null: false
    t.boolean "sfw", default: false, null: false
    t.boolean "published", default: false, null: false
    t.boolean "locked", default: false, null: false
    t.boolean "hidden_in_history", default: false, null: false
    t.bigint "user_id"
    t.bigint "license_id"
    t.bigint "language_id"
    t.datetime "published_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug", null: false
    t.index ["language_id"], name: "index_uploads_on_language_id"
    t.index ["license_id"], name: "index_uploads_on_license_id"
    t.index ["slug"], name: "index_uploads_on_slug", unique: true
    t.index ["user_id"], name: "index_uploads_on_user_id"
  end

  create_table "uploads_tags", id: false, force: :cascade do |t|
    t.bigint "upload_id", null: false
    t.bigint "tag_id", null: false
    t.index ["upload_id", "tag_id"], name: "index_uploads_tags_on_upload_id_and_tag_id", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "username", null: false
    t.string "real_name"
    t.text "biography"
    t.string "role", default: "user", null: false
    t.integer "albums_count", default: 0, null: false
    t.integer "articles_count", default: 0, null: false
    t.integer "notes_count", default: 0, null: false
    t.integer "projects_count", default: 0, null: false
    t.integer "uploads_count", default: 0, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  add_foreign_key "albums", "languages"
  add_foreign_key "albums", "licenses"
  add_foreign_key "albums", "users"
  add_foreign_key "announcements", "users"
  add_foreign_key "articles", "languages"
  add_foreign_key "articles", "licenses"
  add_foreign_key "articles", "users"
  add_foreign_key "comments", "users"
  add_foreign_key "notes", "languages"
  add_foreign_key "notes", "licenses"
  add_foreign_key "notes", "users"
  add_foreign_key "preferences", "users"
  add_foreign_key "projects", "languages"
  add_foreign_key "projects", "licenses"
  add_foreign_key "projects", "users"
  add_foreign_key "reports", "users"
  add_foreign_key "uploads", "languages"
  add_foreign_key "uploads", "licenses"
  add_foreign_key "uploads", "users"
end
